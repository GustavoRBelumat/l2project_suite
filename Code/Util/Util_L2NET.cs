﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO.Compression;

namespace L2Project_Suite
{
    public static partial class Util
    {
        public static void KillThreads()
        {
            Stop_Threads();

            if (Globals.login_window != null && Globals.login_window.IsDisposed == false)
            {
                Globals.login_window.Close();
            }
            if (Globals.map_window != null && Globals.map_window.IsDisposed == false)
            {
                Globals.map_window.Close();
            }

            Util.ClearAllNearby();
            Util.ClearAllSelf();
            Globals.listeningig = false;
        }

        public static void Setup_Threads()
        {
            
            Globals.Login_State = 0;
            Globals.Mixer = null;

            Globals.gamesocket_ready = false;
            Globals.clientsocket_ready = false;

            Globals.gamedata.CreateCrypt();

            Globals.oog_loginthread = new System.Threading.Thread(new System.Threading.ThreadStart(LoginServer.OOG_Login));

            Globals.ig_loginthread = new System.Threading.Thread(new System.Threading.ThreadStart(LoginServer.IG_Login));
            Globals.ig_listener = new System.Threading.Thread(new System.Threading.ThreadStart(LoginServer.IG_Listener));
            Globals.ig_Gamelistener = new System.Threading.Thread(new System.Threading.ThreadStart(LoginServer.IG_StartGameLinks));
            Globals.loginsendthread = new System.Threading.Thread(new System.Threading.ThreadStart(LoginServer.LoginSendThread));
            Globals.loginreadthread = new System.Threading.Thread(new System.Threading.ThreadStart(LoginServer.LoginReadThread));


            Globals.gameprocessdatathread = new System.Threading.Thread(new System.Threading.ThreadStart(GameServer.ProcessDataThread));

            Globals.gamedrawthread = new System.Threading.Thread(new System.Threading.ThreadStart(MapThread.DrawGameThread));

            Globals.gamethread = new ServerThread();
            Globals.clientthread = new ClientThread();

            Globals.botaithread = new BotAIThread();
            //Oddi: Follow Rest thread
            Globals.followrestthread = new FollowRestThread();
            Globals.scriptthread = new ScriptEngine();



            //this should mnake all the threads close once the foreground threads close
            Globals.oog_loginthread.IsBackground = true;

            Globals.ig_loginthread.IsBackground = true;
            Globals.ig_listener.IsBackground = true;
            Globals.ig_Gamelistener.IsBackground = true;
            Globals.loginsendthread.IsBackground = true;
            Globals.loginreadthread.IsBackground = true;

            Globals.gameprocessdatathread.IsBackground = true;

            Globals.gamedrawthread.IsBackground = true;
            if (Globals.listeningig == false)
            Login.CallListen();



        }

        public static void Stop_Connections()
        {
            Globals.l2net_home.Add_Text("Killing all network connections");

            KillClientGameConnections();

            KillServerGameConnections();

            //////////////////////////////

            KillServerLoginConnections();

            KillClientLoginConnections();
        }

        public static void KillClientGameConnections()
        {
            try
            {
                if (Globals.Game_ClientLink != null)
                {
                    Globals.Game_ClientLink.Stop();
                }
            }
            catch//(Exception ex)
            {
                //Globals.l2net_home.Add_Error("Game_ClientLink : " + ex.Message);
            }

            Globals.Game_ClientLink = null;

            try
            {
                if (Globals.Game_ClientSocket != null)
                {
                    Globals.Game_ClientSocket.Close();
                }
            }
            catch//(Exception ex)
            {
                //Globals.l2net_home.Add_Error("Game_ClientSocket : " + ex.Message);
            }

            Globals.Game_ClientSocket = null;
        }

        public static void KillServerGameConnections()
        {
            try
            {
                if (Globals.Game_GameSocket != null)
                {
                    Globals.Game_GameSocket.Close();
                }
            }
            catch//(Exception ex)
            {
                //Globals.l2net_home.Add_Error("Game_GameLink : " + ex.Message);
            }

            Globals.Game_GameSocket = null;
        }

        public static void KillServerLoginConnections()
        {
            try
            {
                if (Globals.Login_GameSocket != null)
                {
                    Globals.Login_GameSocket.Shutdown(System.Net.Sockets.SocketShutdown.Both);
                    Globals.Login_GameSocket.Close();
                }
            }
            catch//(Exception ex)
            {
                //Globals.l2net_home.Add_Error("Login_GameLink : " + ex.Message);
            }

            Globals.Login_GameSocket = null;
        }

        public static void KillClientLoginConnections()
        {
            try
            {
                if (Globals.Login_ClientLink != null)
                {
                    Globals.Login_ClientLink.Stop();
                }
            }
            catch//(Exception ex)
            {
                //Globals.l2net_home.Add_Error("Login_ClientLink : " + ex.Message);
            }

            Globals.Login_ClientLink = null;

            try
            {
                if (Globals.Login_ClientSocket != null)
                {
                    Globals.Login_ClientSocket.Shutdown(System.Net.Sockets.SocketShutdown.Both);
                    Globals.Login_ClientSocket.Close();
                }
            }
            catch//(Exception ex)
            {
                //Globals.l2net_home.Add_Error("Login_ClientSocket : " + ex.Message);
            }

            Globals.Login_ClientSocket = null;
        }

        public static void Stop_Threads()
        {
            Globals.l2net_home.Add_Text("Killing all threads");

            try
            {
                if (Globals.oog_loginthread != null)
                    Globals.oog_loginthread.Abort();
            }
            catch//(Exception e)
            {
                //Globals.l2net_home.Add_Error("oog_loginthread : " + e.Message);
            } 
            
            try
            {
                if (Globals.ig_loginthread != null)
                    Globals.ig_loginthread.Abort();
            }
            catch//(Exception e)
            {
                //Globals.l2net_home.Add_Error("ig_loginthread : " + e.Message);
            }

            try
            {
                if (Globals.ig_listener != null)
                    Globals.ig_listener.Abort();
            }
            catch//(Exception e)
            {
                //Globals.l2net_home.Add_Error("ig_listener : " + e.Message);
            }

            try
            {
                if (Globals.ig_Gamelistener != null)
                    Globals.ig_Gamelistener.Abort();
            }
            catch//(Exception e)
            {
                //Globals.l2net_home.Add_Error("ig_Gamelistener : " + e.Message);
            }

            try
            {
                if (Globals.loginsendthread != null)
                    Globals.loginsendthread.Abort();
            }
            catch//(Exception e)
            {
                //Globals.l2net_home.Add_Error("loginsendthread : " + e.Message);
            }

            try
            {
                if (Globals.loginreadthread != null)
                    Globals.loginreadthread.Abort();
            }
            catch//(Exception e)
            {
                //Globals.l2net_home.Add_Error("loginreadthread : " + e.Message);
            }

            try
            {
                if (Globals.gameprocessdatathread != null)
                    Globals.gameprocessdatathread.Abort();
            }
            catch//(Exception e)
            {
                //Globals.l2net_home.Add_Error("gameprocessdatathread : " + e.Message);
            }

            try
            {
                if (Globals.gamedrawthread != null)
                    Globals.gamedrawthread.Abort();
            }
            catch//(Exception e)
            {
                //Globals.l2net_home.Add_Error("gamedrawthread : " + e.Message);
            }

            try
            {
                if (Globals.gamethread != null)
                    Globals.gamethread.readthread.Abort();
            }
            catch//(Exception e)
            {
                //Globals.l2net_home.Add_Error("gamethread.readthread : " + e.Message);
            }

            try
            {
                if (Globals.gamethread != null)
                    Globals.gamethread.sendthread.Abort();
            }
            catch//(Exception e)
            {
                //Globals.l2net_home.Add_Error("gamethread.sendthread : " + e.Message);
            }

            try
            {
                if (Globals.clientthread != null)
                    Globals.clientthread.readthread.Abort();
            }
            catch//(Exception e)
            {
                //Globals.l2net_home.Add_Error("clientthread.readthread : " + e.Message);
            }

            try
            {
                if (Globals.clientthread != null)
                    Globals.clientthread.sendthread.Abort();
            }
            catch//(Exception e)
            {
                //Globals.l2net_home.Add_Error("clientthread.sendthread : " + e.Message);
            }

            try
            {
                if (Globals.botaithread != null)
                    Globals.botaithread.botaithread.Abort();
            }
            catch//(Exception e)
            {
                //Globals.l2net_home.Add_Error("botaithread : " + e.Message);
            }

            //Oddi: FollowRestTHread
            try
            {
                if (Globals.followrestthread != null)
                    Globals.followrestthread.followrestthread.Abort();
            }
            catch//(Exception e)
            {
                //Globals.l2net_home.Add_Error("followrestthread : " + e.Message);
            }

            try
            {
                if (Globals.scriptthread != null)
                    Globals.scriptthread.scriptthread.Abort();
            }
            catch//(Exception e)
            {
                //Globals.l2net_home.Add_Error("scriptthread : " + e.Message);
            }
        }

        //public static void Check_Version()
        //{
        //    System.Net.HttpWebRequest myReq = (System.Net.HttpWebRequest)System.Net.WebRequest.Create("http://l2net.oddi.org/latest3.txt");

        //    myReq.Timeout = 5000;//set the timeout at 5 seconds instead of 5min
        //    myReq.ReadWriteTimeout = 5000;//lets put a 5 seconds instead of 5min delay

        //    try
        //    {
        //        System.Net.HttpWebResponse myRes = (System.Net.HttpWebResponse)myReq.GetResponse();

        //        System.IO.BinaryReader reader = new System.IO.BinaryReader(myRes.GetResponseStream());

        //        ByteBuffer bbuff = new ByteBuffer(reader.ReadBytes((int)myRes.ContentLength));
        //        reader.Close();

        //        string inp = bbuff.ReadString1B();

        //        int pipe;
        //        //latest_ver
        //        pipe = inp.IndexOf('|');
        //        int latest_ver = Util.GetInt32(inp.Substring(0, pipe));
        //        inp = inp.Remove(0, pipe + 1);
        //        //min_ver
        //        pipe = inp.IndexOf('|');
        //        int min_ver = Util.GetInt32(inp.Substring(0, pipe));
        //        inp = inp.Remove(0, pipe + 1);
        //        //pop_up
        //        pipe = inp.IndexOf('|');
        //        int pop_up = Util.GetInt32(inp.Substring(0, pipe));
        //        inp = inp.Remove(0, pipe + 1);
        //        //dupe keys
        //        pipe = inp.IndexOf('|');
        //        int dupe_keys = Util.GetInt32(inp.Substring(0, pipe));
        //        inp = inp.Remove(0, pipe + 1);
        //        //popup timer
        //        pipe = inp.IndexOf('|');
        //        int ptimer = Util.GetInt32(inp.Substring(0, pipe));
        //        inp = inp.Remove(0, pipe + 1);

        //        ////latest version
        //        //if (Util.GetInt32(Globals.Version) < latest_ver)
        //        //{
        //        //    Globals.l2net_home.Add_Text("You are running an old copy of L2Net. v" + latest_ver.ToString() + " is available at wwww.insane-gamers.com", System.Drawing.Brushes.Gold, TextType.ALL);
        //        //}

        //        ////if below the latest version... force an upgrade
        //        //if (Util.GetInt32(Globals.Version) < min_ver)
        //        //{
        //        //    System.Windows.Forms.MessageBox.Show("You NEED to upgrade to at least version " + min_ver.ToString() + Environment.NewLine + "The application will now be forced to close");
        //        //    System.Windows.Forms.Application.Exit();
        //        //    Globals.l2net_home.Dispose();
        //        //}

        //        if (!Globals.isValidKey)
        //        {
        //            //pop up ads... woot
        //            if (pop_up != 0)
        //            {
        //                Globals.PopUpAds = true;
        //            }
        //            else
        //            {
        //                Globals.PopUpAds = false;
        //            }

        //            //dupe keys
        //            if (dupe_keys != 0)
        //            {
        //                Globals.NoDupeKeys = true;
        //            }
        //            else
        //            {
        //                Globals.NoDupeKeys = false;
        //            }

        //            Globals.PopUp_Timer = ptimer * System.TimeSpan.TicksPerMinute;
        //        }
        //    }
        //    catch
        //    {
        //        Globals.l2net_home.Add_Error("L2.Net latest version check timed out. This can be caused by many things including: that gay porn you are seeding.");
        //        //cant do anything about the min version check
        //        if (!Globals.isValidKey)
        //        {
        //            Globals.PopUpAds = true;
        //            Globals.NoDupeKeys = true;
        //            Globals.RunDegraded = true;
        //        }
        //    }
        //}

        public static void Get_Banned()
        {
            Globals.Bannedkeys.Add("XXCELFQXQWODTQRR");//lynn burgess
            Globals.Bannedkeys.Add("XXCHHBULMVAJPVVM");//Helaine S Cabral

            System.Net.HttpWebRequest myReq = (System.Net.HttpWebRequest)System.Net.WebRequest.Create("http://l2net.oddi.org/banned.txt");

            myReq.Timeout = 20000;//set the timeout at 20 seconds instead of 5min
            myReq.ReadWriteTimeout = 20000;//lets put a 20 seconds instead of 5min delay

            try
            {
                System.Net.HttpWebResponse myRes = (System.Net.HttpWebResponse)myReq.GetResponse();

                System.IO.BinaryReader reader = new System.IO.BinaryReader(myRes.GetResponseStream());

                byte[] data = reader.ReadBytes((int)myRes.ContentLength);
                reader.Close();

                byte[] dec = AES.Decrypt(data, Globals.AES_Key, "fucking scammers...");

                System.IO.StreamReader temp_stream;
                System.IO.MemoryStream mem_stream;

                mem_stream = new System.IO.MemoryStream(dec);
                temp_stream = new System.IO.StreamReader((System.IO.Stream)mem_stream);

                string key;

                while ((key = temp_stream.ReadLine()) != null)
                {
                    Globals.Bannedkeys.Add(key);
                }
            }
            catch
            {
                //hmmm, must have failed
            }
        }


        public static void Get_Trial_Status()
        {

            System.Net.HttpWebRequest myReq = (System.Net.HttpWebRequest)System.Net.WebRequest.Create("http://l2net.oddi.org/banned2.txt");

            myReq.Timeout = 20000;//set the timeout at 20 seconds instead of 5min
            myReq.ReadWriteTimeout = 20000;//lets put a 20 seconds instead of 5min delay

            try
            {
                System.Net.HttpWebResponse myRes = (System.Net.HttpWebResponse)myReq.GetResponse();

                System.IO.BinaryReader reader = new System.IO.BinaryReader(myRes.GetResponseStream());

                byte[] data = reader.ReadBytes((int)myRes.ContentLength);
                reader.Close();

                byte[] dec = AES.Decrypt(data, Globals.AES_Key, ")&asoihd/&)(#HUAxx");

                System.IO.StreamReader temp_stream;
                System.IO.MemoryStream mem_stream;

                mem_stream = new System.IO.MemoryStream(dec);
                temp_stream = new System.IO.StreamReader((System.IO.Stream)mem_stream);


                string trial = temp_stream.ReadLine();
                Globals.l2net_home.Add_Text("Trial: " + trial);

                if (trial == "thecakeisalie")
                {
                    Globals.l2net_home.Add_Text("L2Net full version event is active. Enjoy :)");
                }

            }
            catch (Exception e)
            {
                Globals.l2net_home.Add_Text("Exeption: " + e.Message);
                //hmmm, must have failed
            }
        }

        //public static void Request_Donation()
        //{
        //    string amount;

        //    System.Net.HttpWebRequest myReq = (System.Net.HttpWebRequest)System.Net.WebRequest.Create("http://l2net.oddi.org/donation.txt");

        //    myReq.Timeout = 2000;//set the timeout at 10 seconds instead of 5min
        //    myReq.ReadWriteTimeout = 2000;//lets put a 10 seconds instead of 5min delay

        //    try
        //    {
        //        System.Net.HttpWebResponse myRes = (System.Net.HttpWebResponse)myReq.GetResponse();

        //        System.IO.BinaryReader reader = new System.IO.BinaryReader(myRes.GetResponseStream());

        //        ByteBuffer bbuff = new ByteBuffer(reader.ReadBytes((int)myRes.ContentLength));
        //        reader.Close();

        //        amount = bbuff.ReadString1B();

        //    }
        //    catch
        //    {
        //        //hmmm, must have failed
        //        //lets ask for a lot of money
        //        amount = "1000";
        //    }

        //    Globals.l2net_home.Add_Text("Hello " + Environment.UserName + "!", Globals.White, TextType.ALL);
        //    Globals.l2net_home.Add_Text("Use of this program is only allowed if the included EULA.txt is accepted.", Globals.White, TextType.ALL);
        //    Globals.l2net_home.Add_Text("Motivation for development comes only through your donation! Click \"Help\" -> \"Donate\" to make your donation today!", Globals.White, TextType.ALL);
        //    if (Util.GetInt32(amount) == 0)
        //    {
        //        //Globals.l2net_home.Add_Text("Please donate!", Globals.White);
        //    }
        //    else
        //    {
        //        Globals.l2net_home.Add_Text("Please donate!  We currently need to raise " + amount + "USD", Globals.White, TextType.ALL);
        //    }
        //    Globals.l2net_home.Add_Text("Thank you for your help, and happy botting!", Globals.White, TextType.ALL);
        //}

        public static void Flush_TextFile()
        {
            if (Globals.text_out != null)
            {
                try
                {
#if DEBUG
                    Globals.gamedataout.Flush();
                    Globals.gamedatato.Flush();
                    Globals.clientdataout.Flush();
                    Globals.clientdatato.Flush();
#endif

                    Globals.text_out.Flush();
                }
                catch
                {
                    //error flushing
                }
            }
        }

        public static void Force_Collect()
        {
            Globals.l2net_home.Add_Text("*******************************");
            Globals.l2net_home.Add_Text("Handle count: " + System.Diagnostics.Process.GetCurrentProcess().HandleCount.ToString());
            Globals.l2net_home.Add_Text("Memory usage / allocated: " + GC.GetTotalMemory(false).ToString() + " / " + Environment.WorkingSet.ToString());
            Globals.l2net_home.Add_Text("Cleaning up memory... this may take a few moments");
            VoicePlayer.PlaySound(7);
            GC.Collect();
            Globals.l2net_home.Add_Text("Clean up complete!");
            Globals.l2net_home.Add_Text("Handle count: " + System.Diagnostics.Process.GetCurrentProcess().HandleCount.ToString());
            Globals.l2net_home.Add_Text("Memory usage / allocated: " + GC.GetTotalMemory(true).ToString() + " / " + Environment.WorkingSet.ToString());
            Globals.l2net_home.Add_Text("*******************************");
        }

        public static void Free_Assets()
        {
            Globals.gamedata.running = false;

            if (Globals.gameprocessdatathread != null && Globals.gameprocessdatathread.ThreadState == System.Threading.ThreadState.Running)
                Globals.gameprocessdatathread.Abort();
            if (Globals.gamedrawthread != null && Globals.gamedrawthread.ThreadState == System.Threading.ThreadState.Running)
                Globals.gamedrawthread.Abort();
            if (Globals.directinputthread != null && Globals.directinputthread.ThreadState == System.Threading.ThreadState.Running)
                Globals.directinputthread.Abort();
            Globals.pck_thread.stop();
            try
            {
#if DEBUG
                Globals.gamedatato.Flush();
                Globals.gamedataout.Flush();
                Globals.clientdatato.Flush();
                Globals.clientdataout.Flush();

                Globals.gamedatato.Close();
                Globals.gamedataout.Close();
                Globals.clientdatato.Close();
                Globals.clientdataout.Close();
#endif
                if (Globals.text_out != null)
                {
                    Globals.text_out.Flush();
                    Globals.text_out.Close();
                }
            }
            catch
            {
                //error closing something...oh well
            }

            if (Globals.Game_GameSocket != null)
            {
                Globals.Game_GameSocket.Close();
            }

            if (Globals.Game_ClientLink != null)
            {
                Globals.Game_ClientLink.Stop();
            }

            //GC.Collect();
        }

        public static void SayToClient(int index, string end)
        {
            uint type = 0;

            switch (index)
            {
                case 0://all 0x00
                    type = 0x00;
                    break;
                case 1://shout 0x01
                    type = 0x01;
                    break;
                case 2://tell player 0x02
                    type = 0x02;
                    break;
                case 3://party 0x03
                    type = 0x03;
                    break;
                case 4://clan 0x04
                    type = 0x04;
                    break;
                case 5://from gm 0x05
                    type = 0x05;
                    break;
                case 6://petition from gm 0x06
                    type = 0x06;
                    break;
                case 7://reply to petition from gm 0x07
                    type = 0x07;
                    break;
                case 8://trade 0x08
                    type = 0x08;
                    break;
                case 9://alliance 0x09
                    type = 0x09;
                    break;
                case 10://announcement 0x0A
                    type = 0x0A;
                    break;
                case 11://crash client 0x0B
                    type = 0x0B;
                    break;
                case 12://fake1 0x0C
                    type = 0x0C;
                    break;
                case 13://fake2 0x0D
                    type = 0x0D;
                    break;
                case 14://fake3 0x0E
                    type = 0x0E;
                    break;
                case 15://party room all 0x0F
                    type = 0x0F;
                    break;
                case 16://party room commander 0x10
                    type = 0x10;
                    break;
                case 17://hero 0x11
                    type = 0x11;
                    break;
            }

            string start = "=L2.Net=";

            //need to build a packet and send it out
            int startlen = start.Length * 2 + 2;
            int endlen = end.Length * 2;
            if (endlen > 0)
                endlen += 2;
            ByteBuffer bbuff;
            if (Globals.gamedata.Chron >= Chronicle.CT2_6)
            {
                bbuff = new ByteBuffer(13 + startlen + endlen);
            }
            else
            {
                bbuff = new ByteBuffer(9 + startlen + endlen);
            }
            bbuff.WriteByte((byte)PServer.CreatureSay);
            bbuff.WriteUInt32(0);
            bbuff.WriteUInt32(type);
            bbuff.WriteString(start);
            if (Globals.gamedata.Chron >= Chronicle.CT2_6)
            {
                bbuff.WriteInt32(-1);
            }
            bbuff.WriteString(end);

            Globals.gamedata.SendToClient(bbuff);
        }

        public static void Open_PopUp()
        {
#if !DEBUG
            System.Threading.Thread pthread = new System.Threading.Thread(new System.Threading.ThreadStart(Open_PopUp_Inner));
            pthread.IsBackground = true;
            pthread.Start();
#endif
        }

        private static void Open_PopUp_Inner()
        {
            try
            {
                //System.Diagnostics.Process.Start("http://l2net.insane-gamers.com/popup.html");
            }
            catch
            {
                //problem opening browser?
            }
        }

        public static void PopUp_Check()
        {
            if (Globals.PopUpAds && (System.DateTime.Now.Ticks > Globals.Last_PopUp + Globals.PopUp_Timer))
            {
                Open_PopUp();
                Globals.Last_PopUp = System.DateTime.Now.Ticks;
            }
        }

        public static int GCheckKey(int i)
        {
            if (Globals.KeyCheckCount == 0)
            {
                Util.Kill();
                return i;
            }

            if (Globals.isValidKey)
            {
                Globals.l2net_home.Add_Text("VALID PRODUCT KEY - Running in Full Mode");
                return i + 1;
            }
            else
            {
                //Globals.l2net_home.Add_Error("INVALID PRODUCT KEY - Running in Normal Mode", false);
                return i - 1;
            }
        }

        public static void Kill()
        {
            Util.KillThreads();
            System.Windows.Forms.Application.Exit();
        }
    }
}
