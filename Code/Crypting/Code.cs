using System;

namespace L2Project_Suite
{
	/// <summary>
	/// Summary description for Code.
	/// </summary>
	public class Code
	{
		public Code()
		{
		}

		public static void isValidKey()
		{
            if(isValidKeyInternal(Globals.ProductKey))
            {
                Globals.PopUpAds = false;
                Globals.RunDegraded = false;
                Globals.isValidKey = true;
            }
            else
            {
                Globals.PopUpAds = true;
                Globals.RunDegraded = true;
                Globals.isValidKey = false;
            }
		}

        private static bool isValidKeyInternal(string key)
        {
            if (key == "1111")
            {
                DateTime date = Util.GetNISTDate(true);
                DateTime localdate = DateTime.Now;
                DateTime expdate = new DateTime(2012, 01, 01);
                if (((date.Day <= expdate.Day) && (localdate.Day <= expdate.Day) && (date.Year <= expdate.Year) && (localdate.Year <= expdate.Year)))
                {
                    Globals.l2net_home.Add_Text("Promotion key activated, enjoy your full version trial :)");
                    Globals.l2net_home.Add_Text((expdate.Date - date.Date).ToString() + " days left of promotion");
                    return true;
                }
                return false;
            }
            else
            {
                Globals.KeyCheckCount++;

                if (key.Length != 16)
                    return false;

                foreach (string banned in Globals.Bannedkeys)
                {
                    if (System.String.Equals(banned, key))
                    {
                        System.Windows.Forms.MessageBox.Show("banned key");
                        System.Windows.Forms.Application.Exit();
                    }
                }

                string key_enc = Globals.Code_Key;

                int[] key_int = new int[16];
                int i1, i2;

                for (i1 = 0; i1 < 16; i1++)
                {
                    key_int[i1] = ClampMinus(key[i1], key_enc[i1]);
                }

                for (i1 = 0; i1 < 16; i1++)
                {
                    for (i2 = 0; i2 < 16; i2++)
                    {
                        if (i1 != i2)
                            if (key_int[i1] == key_int[i2])
                                return false;
                    }
                }

                for (i1 = 0; i1 < 16; i1++)
                {
                    for (i2 = 0; i2 < 16; i2++)
                    {
                        if (i1 != i2)
                            if (System.Math.Abs(key_int[i1] - key_int[i2]) == System.Math.Abs(i1 - i2))
                                return false;
                    }
                }

                return true;
            }
        }

		private static char ClampMinus(int a, int b)
		{
			int x = (int)a - (int)b;
			while(x>25)
				x-=26;
			while(x<0)
				x+=26;
			char c = (char)x;
			return c;
		}
	}
}
