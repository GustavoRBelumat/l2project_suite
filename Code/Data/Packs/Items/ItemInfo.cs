using System;

namespace L2Project_Suite
{
	public class ItemInfo : Object_Base
	{
        public volatile uint ItemID = 0;//this is the item id from the file
        public volatile float X = 0;
        public volatile float Y = 0;
        public volatile float Z = 0;
        public volatile uint Stackable = 0;
        public volatile uint Count = 0;
        public volatile uint DroppedBy = 0;
        public volatile float DropRadius = 2.0F;
        public volatile bool Ignore = false;
        public volatile bool IsMine = false;
        public volatile bool HasMesh = false;

		public void Load(ByteBuffer buff)
		{
			ID =	buff.ReadUInt32();
			ItemID =buff.ReadUInt32();
			X =		buff.ReadInt32();
			Y =		buff.ReadInt32();
			Z =		buff.ReadInt32();
			Stackable = buff.ReadUInt32();
			Count =	buff.ReadUInt32();

            HasMesh = Util.GetItemHasMesh(ItemID);
		}

		public void LoadDrop(ByteBuffer buff)
		{
			DroppedBy =	buff.ReadUInt32();
			ID =		buff.ReadUInt32();
			ItemID =	buff.ReadUInt32();
			X =			buff.ReadInt32();
			Y =			buff.ReadInt32();
			Z =			buff.ReadInt32();
			Stackable =	buff.ReadUInt32();
			Count =		buff.ReadUInt32();
			buff.ReadUInt32();//unknown 4 bytes after this

            HasMesh = Util.GetItemHasMesh(ItemID);
        }
	}
}
