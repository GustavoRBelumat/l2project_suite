using System;

namespace L2Project_Suite
{
    public enum InventorySlots : uint
    {
        Shirt = 0x01,
        Ear = 0x06,
        Neck = 0x08,
        Finger = 0x30,
        Head = 0x40,
        RHand = 0x80,
        LHand = 0x100,
        Gloves = 0x200,
        Chest = 0x400,
        Pants = 0x800,
        Feet = 0x1000,
        Dunno = 0x2000,
        LRHand = 0x4000,
        FullBody = 0x8000,
        Accessory = 0x40000,
    }

    public enum InventoryElement : int
    {
        None = -1,
        Fire = 0,
        Water = 1,
        Wind = 2,
        Earth = 3,
        Holy = 4,
        Unholy = 5,
    }

	public class InventoryInfo : Object_Base
	{
        public volatile uint Type = 0;//ushort
        public volatile uint ItemID = 0;//this is the id from the file
        private ulong _Count = 0;
        public volatile uint Type2 = 0;
        public volatile uint Type3 = 0;//ushort
        public volatile uint isEquipped = 0;//0x01 equipped | 0x00 not//ushort
        public volatile InventorySlots Slot = 0;
        public volatile uint Enchant = 0;//ushort
        public volatile uint Type4 = 0;//ushort
        public volatile uint AugID = 0;//C6
        public volatile uint Mana = 0;//C6
        public volatile bool InNewList = true;

        public volatile InventoryElement attack_element;
        public volatile int attack_magnitude = 0;
        public volatile int fire_defense = 0;
        public volatile int water_defense = 0;
        public volatile int wind_defense = 0;
        public volatile int earth_defense = 0;
        public volatile int unholy_defense = 0;
        public volatile int holy_defense = 0;
        public volatile uint location_slot = 0;
        public volatile int lease_time = 0;
        public volatile int enchant_effect_0 = 0;
        public volatile int enchant_effect_1 = 0;
        public volatile int enchant_effect_2 = 0;

		private readonly object CountLock = new object();

		public ulong Count
		{
			get
			{
                ulong tmp;
				lock(CountLock)
				{
					tmp = this._Count;
				}
				return tmp;
			}
			set
			{
				lock(CountLock)
				{
					_Count = value;
				}
			}
		}

		public void Load(ByteBuffer buff)
		{
            Type = buff.ReadUInt16();
            ID = buff.ReadUInt32();
            ItemID = buff.ReadUInt32();
            Count = buff.ReadUInt32();
            Type2 = buff.ReadUInt32();
            isEquipped = buff.ReadUInt16();
            Slot = (InventorySlots)buff.ReadUInt32();
            Enchant = buff.ReadUInt16();
            //Type2 = buff.ReadUInt16();
            buff.ReadUInt16(); //Type4
            AugID = buff.ReadUInt32();//C6
            buff.ReadUInt32(); //mana
        }
	}
}
