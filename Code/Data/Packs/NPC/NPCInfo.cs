using System;

namespace L2Project_Suite
{
	public class NPCInfo : Object_Base
	{
		public volatile uint NPCID = 0;//this it the NPC type (from the file)
        public volatile uint isAttackable = 0;//0 = invincible... non zero = attackable
        public volatile float X = 0;
        public volatile float Y = 0;
        public volatile float Z = 0;
        public volatile int Heading = 0;
        public volatile uint MatkSpeed = 0;
        public volatile uint PatkSpeed = 0;
        public volatile uint Level = 0;

        public volatile float RunSpeed = 0;
        public volatile float WalkSpeed = 0;
        public volatile uint SwimRunSpeed = 0;
        public volatile uint SwimWalkSpeed = 0;
        public volatile uint flRunSpeed = 0;
        public volatile uint flWalkSpeed = 0;
        public volatile uint FlyRunSpeed = 0;
        public volatile uint FlyWalkSpeed = 0;

        public volatile float MoveSpeedMult = 0;
        public volatile float AttackSpeedMult = 0;
        public volatile float CollisionRadius = 0;
        public volatile float CollisionHeight = 0;

        public volatile uint RHand = 0;
        public volatile uint LRHand = 0;//unknown, maybe correct
        public volatile uint LHand = 0;
        public volatile uint NameShows = 0;//byte

        public volatile uint isRunning = 1;// running = 1   walking = 0//byte
        public volatile uint isSitting = 1;
        public volatile uint isInCombat = 0;//byte
        private volatile uint _isAlikeDead = 0;//byte
        private volatile uint _isInvisible = 0;//0 - false | 1 - true | 2 - summoned//byte

		private string _Name = "";
		private string _Title = "";
        public volatile uint Karma = 0;//maybe
        public volatile uint PvPFlag = 0;//maybe
        public volatile uint SummonedNameColor = 0;//maybe

        public volatile uint AbnormalEffects = 0;
        public volatile uint ExtendedEffects = 0;
        public volatile uint TeamCircle = 0;//byte

        public volatile float Dest_X = 0;
        public volatile float Dest_Y = 0;
        public volatile float Dest_Z = 0;
        public volatile bool Moving = false;
		private System.DateTime _lastMoveTime = System.DateTime.Now;
        public volatile uint MoveTarget = 0;
        public volatile TargetType MoveTargetType = TargetType.NONE;

        public volatile uint TargetID = 0;
        public volatile TargetType CurrentTargetType = TargetType.NONE;

        public volatile float Cur_HP = 0;
        public volatile float Max_HP = 0;
        public volatile float Cur_MP = 0;
        public volatile float Max_MP = 0;
        public volatile float Cur_CP = 0;
        public volatile float Max_CP = 0;

        private long _RemoveAt = long.MaxValue;
        public volatile bool Persist = false;
        public volatile bool Ignore = false;
        public volatile bool IsSpoiled = false;

        private readonly object RemoveAtLock = new object();
        private readonly object lastMoveTimeLock = new object();
		private readonly object TitleLock = new object();
		private readonly object NameLock = new object();

        public bool HasEffect(AbnormalEffects test)
        {
            return (AbnormalEffects & (uint)test) != 0;
        }

        public bool HasExtendedEffect(ExtendedEffects test)
        {
            return (ExtendedEffects & (uint)test) != 0;
        }

        public bool PriorityTarget()
        {
            if (this.TargetID == 0)
            {
                return false;
            }

            //check if targeting any party member
            Globals.PartyLock.EnterReadLock();
            try
            {
                if (Globals.gamedata.PartyMembers.ContainsKey(this.TargetID))
                {
                    return true;
                }
            }
            finally
            {
                Globals.PartyLock.ExitReadLock();
            }

            //check if targeting any out of party member
            if (Globals.gamedata.botoptions.OOP == 1 && Globals.gamedata.botoptions.OOPIDs.Contains(this.TargetID))
            {
                return true;
            }

            return false;
        }

		public bool CheckCombat()
		{
			//if we are in combat, check against player and party
			if(isInCombat == 0)
			{
				return false;
			}
			else
			{
                //need to change this...
                //currently... not in combat if target is targeting us, friendly, or npc
                //if we are targeting the player return false

                if (this.TargetID == Globals.gamedata.my_char.ID)
                {
                    return false;
                }

                if (PriorityTarget())
                {
                    return false;
                }

                CharInfo char_target = null;

                Globals.PlayerLock.EnterReadLock();
                try
                {
                    char_target = Util.GetChar(TargetID);
                }
                finally
                {
                    Globals.PlayerLock.ExitReadLock();
                }

                if (char_target != null)
                {
                    return true;
                }

				return false;
			}
		}

        public void Active()
        {
            if (isAttackable == 0x00 || Persist)
            {
                //town npc...
                RemoveAt = System.DateTime.Now.Ticks + Globals.NPC_RemoveAtInvin;
            }
            else
            {
                RemoveAt = System.DateTime.Now.Ticks + Globals.NPC_RemoveAtActive;
            }
        }

        public long RemoveAt
        {
            get
            {
                long tmp;
                lock (RemoveAtLock)
                {
                    tmp = this._RemoveAt;
                }
                return tmp;
            }
            set
            {
                lock (RemoveAtLock)
                {
                    _RemoveAt = value;
                }
            }
        }
        public uint isAlikeDead
        {
            get
            {
                return _isAlikeDead;
            }
            set
            {
                _isAlikeDead = value;

                if (_isAlikeDead == 1)
                {
                    RemoveAt = System.DateTime.Now.Ticks + Globals.NPC_RemoveAtDead;
                }
                else
                {
                    Active();
                }
            }
        }
        public uint isInvisible
		{
			get
			{
                return _isInvisible;
			}
			set
			{
                _isInvisible = value;

                if (_isInvisible == 0)
                {
                    Active();
                }
                else
                {
                    RemoveAt = System.DateTime.Now.Ticks + Globals.NPC_RemoveAtDead;
                }
			}
		}
		public string Name
		{
			get
			{
				string tmp;
				lock(NameLock)
				{
					tmp = this._Name;
				}
				return tmp;
			}
			set
			{
				lock(NameLock)
				{
					_Name = value;
				}
                Active();
			}
		}
		public string Title
		{
			get
			{
				string tmp;
				lock(TitleLock)
				{
					tmp = this._Title;
				}
				return tmp;
			}
			set
			{
				lock(TitleLock)
				{
					_Title = value;
				}
                Active();
			}
		}
		public System.DateTime lastMoveTime
		{
			get
			{
				System.DateTime tmp;
				lock(lastMoveTimeLock)
				{
					tmp = this._lastMoveTime;
				}
				return tmp;
			}
			set
			{
				lock(lastMoveTimeLock)
				{
					_lastMoveTime = value;
				}
			}
		}

        public void Copy(NPCInfo copy)
        {
            NPCID = copy.NPCID;
            isAttackable = copy.isAttackable;
            X = copy.X;
            Y = copy.Y;
            Z = copy.Z;
            Heading = copy.Heading;
            MatkSpeed = copy.MatkSpeed;
            PatkSpeed = copy.PatkSpeed;

            RunSpeed = copy.RunSpeed;
            WalkSpeed = copy.WalkSpeed;
            SwimRunSpeed = copy.SwimRunSpeed;
            SwimWalkSpeed = copy.SwimWalkSpeed;
            flRunSpeed = copy.flRunSpeed;
            flWalkSpeed = copy.flWalkSpeed;
            FlyRunSpeed = copy.FlyRunSpeed;
            FlyWalkSpeed = copy.FlyWalkSpeed;

            MoveSpeedMult = copy.MoveSpeedMult;
            AttackSpeedMult = copy.AttackSpeedMult;
            CollisionRadius = copy.CollisionRadius;
            CollisionHeight = copy.CollisionHeight;

            RHand = copy.RHand;
            LRHand = copy.LRHand;
            LHand = copy.LHand; 
            NameShows = copy.NameShows;

            isRunning = copy.isRunning;
            isInCombat = copy.isInCombat;
            isAlikeDead = copy.isAlikeDead;
            isInvisible = copy.isInvisible;

            Name = copy.Name;
            Title = copy.Title;

            Karma = copy.Karma;
            PvPFlag = copy.PvPFlag;
            SummonedNameColor = copy.SummonedNameColor;

            AbnormalEffects = copy.AbnormalEffects;
            ExtendedEffects = copy.ExtendedEffects;
        }

        public void Load(ByteBuffer buff)
        {
            //ct2.2: 0C D5 5B 30 4F A6 95 0F 00 01 00 00 00 86 F2 01 00 54 68 FF FF 30 F2 FF FF 00 00 00 00 00 00 00 00 4D 01 00 00 16 01 00 00 AF 00 00 00 2C 00 00 00 AF 00 00 00 2C 00 00 00 AF 00 00 00 2C 00 00 00 AF 00 00 00 2C 00 00 00 9A 99 99 99 99 99 F1 3F 81 43 A8 52 B3 07 F0 3F 00 00 00 00 00 00 30 40 00 00 00 00 00 00 43 40 C8 09 00 00 00 00 00 00 06 1B 00 00 01 01 00 00 01 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 30 40 00 00 00 00 00 00 43 40 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
            //int offset = 1;
            ID = buff.ReadUInt32();
            NPCID = buff.ReadUInt32();
            isAttackable = buff.ReadUInt32();

            X = buff.ReadInt32();
            Y = buff.ReadInt32();
            Z = buff.ReadInt32();
            Dest_X = X;
            Dest_Y = Y;
            Dest_Z = Z;

            Heading = buff.ReadInt32();
            buff.ReadUInt32();
            MatkSpeed = buff.ReadUInt32();
            PatkSpeed = buff.ReadUInt32();

            RunSpeed = buff.ReadUInt32();
            WalkSpeed = buff.ReadUInt32();
            SwimRunSpeed = buff.ReadUInt32();
            SwimWalkSpeed = buff.ReadUInt32();
            flRunSpeed = buff.ReadUInt32();
            flWalkSpeed = buff.ReadUInt32();
            FlyRunSpeed = buff.ReadUInt32();
            FlyWalkSpeed = buff.ReadUInt32();

            MoveSpeedMult = System.Convert.ToSingle(buff.ReadDouble());
            AttackSpeedMult = System.Convert.ToSingle(buff.ReadDouble());
            CollisionRadius = System.Convert.ToSingle(buff.ReadDouble());
            CollisionHeight = System.Convert.ToSingle(buff.ReadDouble());

            RHand = buff.ReadUInt32();
            LRHand = buff.ReadUInt32();
            LHand = buff.ReadUInt32();
            NameShows = buff.ReadByte();

            isRunning = buff.ReadByte();
            isInCombat = buff.ReadByte();
            isAlikeDead = buff.ReadByte();
            if (Globals.gamedata.Chron >= Chronicle.CT2_2)
            {
                buff.ReadByte();
                isInvisible = 0;
            }
            else
            {
                isInvisible = buff.ReadByte(); //l2j writeC(_isSummoned ? 2 : 0); //  0=teleported  1=default   2=summoned
            }

            if (Globals.gamedata.Chron >= Chronicle.CT3_0)
            {
                buff.ReadUInt32(); //FF FF FF FF 
            }

            Name = buff.ReadString();//Name
            if (Name.Length == 0)
            {
                Name = Util.GetNPCName(NPCID);
            }

            if (Globals.gamedata.Chron >= Chronicle.CT3_0)
            {
                buff.ReadUInt32(); //FF FF FF FF 
            }

            Title = buff.ReadString();

            SummonedNameColor = buff.ReadUInt32();//0 = normal, 1 = summoned name color
            PvPFlag = buff.ReadUInt32();
            Karma = buff.ReadUInt32();

            AbnormalEffects = buff.ReadUInt32();
            buff.ReadUInt32(); //clan ID
            buff.ReadUInt32(); //crest ID
            
            if (Globals.gamedata.Chron >= Chronicle.CT2_5)
            {
                buff.ReadUInt32();                      // 00 00 00 00
                buff.ReadUInt32();                      // 00 00 00 00
                buff.ReadByte();
                TeamCircle = buff.ReadByte();
                buff.ReadDouble();                      // col height
                buff.ReadDouble();                      // col rad
                buff.ReadInt32();                       // 00 00 00 00
                buff.ReadInt32();                       // 00 00 00 00
                buff.ReadInt32();                       // 00 00 00 00
                buff.ReadInt32();                       // 00 00 00 00
                buff.ReadByte();                        // 01
                buff.ReadByte();                        // 01
                ExtendedEffects = buff.ReadUInt32();    // 00 00 00 00
                buff.ReadInt32();                       // 00 00 00 00

                if (Globals.gamedata.Chron >= Chronicle.CT3_0)
                {
                    buff.ReadInt32(); //00 00 00 00
                    buff.ReadInt32(); //6C 00 00 00 
                    buff.ReadInt32(); //6C 00 00 00 
                    buff.ReadInt32(); //4C 00 00 00 
                    buff.ReadInt32(); //4C 00 00 00 
                    buff.ReadInt32(); //00 00 00 00
                    buff.ReadInt32(); //00 00 00 00
                    buff.ReadInt32(); //00 00 00 00
                    buff.ReadByte(); //00
                }
            }
            else
            {

                buff.ReadByte(); //?

                TeamCircle = buff.ReadByte();
                buff.ReadDouble();//col height
                buff.ReadDouble();//col rad
                buff.ReadInt32();//c4

                try
                {
                    buff.ReadInt32();//c4
                    buff.ReadInt32();
                    if (Globals.gamedata.Chron >= Chronicle.CT1_5)
                    {
                        buff.ReadInt32();//pet form and skills
                    }
                    if (Globals.gamedata.Chron >= Chronicle.CT2_3)
                    {   //6 more bytes of stuff
                        buff.ReadByte(); //1
                        buff.ReadByte(); //1
                        ExtendedEffects = buff.ReadUInt32();
                    }
                }
                catch
                {
                }
            }
        }

        public void LoadServerObject(ByteBuffer buff)
        {
            ID = buff.ReadUInt32();
            NPCID = buff.ReadUInt32();
            Name = buff.ReadString();
            if (Name.Length == 0)
            {
                Name = Util.GetNPCName(NPCID);
            }
            isAttackable = buff.ReadUInt32();

            X = buff.ReadInt32();
            Y = buff.ReadInt32();
            Z = buff.ReadInt32();

            buff.ReadInt32();

            MoveSpeedMult = System.Convert.ToSingle(buff.ReadDouble());
            AttackSpeedMult = System.Convert.ToSingle(buff.ReadDouble());
            CollisionRadius = System.Convert.ToSingle(buff.ReadDouble());
            CollisionHeight = System.Convert.ToSingle(buff.ReadDouble());

            Cur_HP = buff.ReadUInt32();
            Max_HP = buff.ReadUInt32();

            buff.ReadInt32();//type
            buff.ReadInt32();

            Persist = true;
        }

        public void LoadStaticObject(ByteBuffer buff)
        {
            ID = buff.ReadUInt32();
            NPCID = buff.ReadUInt32();
            Name = buff.ReadString();
            if (Name.Length == 0)
            {
                Name = Util.GetNPCName(NPCID);
            }
            isAttackable = buff.ReadUInt32();

            X = buff.ReadInt32();
            Y = buff.ReadInt32();
            Z = buff.ReadInt32();

            buff.ReadInt32();//heading

            MoveSpeedMult = System.Convert.ToSingle(buff.ReadDouble());
            AttackSpeedMult = System.Convert.ToSingle(buff.ReadDouble());
            CollisionRadius = System.Convert.ToSingle(buff.ReadDouble());
            CollisionHeight = System.Convert.ToSingle(buff.ReadDouble());

            Cur_HP = buff.ReadUInt32();
            Max_HP = buff.ReadUInt32();

            buff.ReadInt32();//type
            buff.ReadInt32();

            Persist = true;
        }

		public void Update(ByteBuffer buff)
		{
			uint data = buff.ReadUInt32();//data = System.BitConverter.ToUInt32(buff,offset);
				
			switch(data)
			{
				case 0x01://level
                    Level = buff.ReadUInt32();
					break;
				case 0x02://exp
                    buff.ReadUInt64();
					break;
				case 0x03://str
                    buff.ReadUInt32();
					break;
				case 0x04://dex
                    buff.ReadUInt32();
					break;
				case 0x05://con
                    buff.ReadUInt32();
					break;
				case 0x06://int
                    buff.ReadUInt32();
					break;
				case 0x07://wit
                    buff.ReadUInt32();
					break;
				case 0x08://men
                    buff.ReadUInt32();
					break;
				case 0x09://cur hp
                    Cur_HP = buff.ReadUInt32();
					break;
				case 0x0A://max hp
                    Max_HP = buff.ReadUInt32();
					break;
				case 0x0B://cur mp
                    Cur_MP = buff.ReadUInt32();
					break;
				case 0x0C://max mp
                    Max_MP = buff.ReadUInt32();
					break;
				case 0x0D://sp
                    buff.ReadUInt32();
					break;
				case 0x0E://cur load
                    buff.ReadUInt32();
					break;
				case 0x0F://max load
                    buff.ReadUInt32();
					break;
				case 0x10://..
                    buff.ReadUInt32();
					break;
				case 0x11://patk
                    buff.ReadUInt32();
					break;
				case 0x12://atk spd
                    PatkSpeed = buff.ReadUInt32();
					break;
				case 0x13://pdef
                    buff.ReadUInt32();
					break;
				case 0x14://evasion
                    buff.ReadUInt32();
					break;
				case 0x15://acc
                    buff.ReadUInt32();
					break;
				case 0x16://crit
                    buff.ReadUInt32();
					break;
				case 0x17://m atk
                    buff.ReadUInt32();
					break;
				case 0x18://cast spd
                    MatkSpeed = buff.ReadUInt32();
					break;
				case 0x19://mdef
                    buff.ReadUInt32();
					break;
				case 0x1A://pvp flag
                    PvPFlag = buff.ReadUInt32();
					break;
				case 0x1B://karma
                    Karma = buff.ReadUInt32();
					break;
				case 0x1C://..
                    buff.ReadUInt32();
					break;
				case 0x1D://..
                    buff.ReadUInt32();
					break;
				case 0x1E://..
                    buff.ReadUInt32();
					break;
				case 0x1F://..
                    buff.ReadUInt32();
					break;
				case 0x20://..
                    buff.ReadUInt32();
					break;
				case 0x21://cur cp
                    Cur_CP = buff.ReadUInt32();
					break;
				case 0x22://max cp
                    Max_CP = buff.ReadUInt32();
					break;
                default:
                    buff.ReadUInt32();
                    break;
			}

            Active();
		}
	}//end of NPCInfo
}
