using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace L2Project_Suite
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.DoEvents();

            Control.CheckForIllegalCrossThreadCalls = false;
            Application.Run(new Suite(args));
        }
    }
}